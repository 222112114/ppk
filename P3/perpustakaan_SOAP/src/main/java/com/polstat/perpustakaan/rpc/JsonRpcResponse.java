/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.polstat.perpustakaan.rpc;

import com.polstat.perpustakaan.dto.BookDto;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author IRGI FAHROZI
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class JsonRpcResponse {

    private String jsonrpc;
    private Object result;
    private Object error;
    private String id;

    public JsonRpcResponse(List<BookDto> result, String id) {
        this.result = result;
        this.id = id;
    }
    
    public JsonRpcResponse(Object result, String id) {
        this.result = result;
        this.id = id;
    }
}

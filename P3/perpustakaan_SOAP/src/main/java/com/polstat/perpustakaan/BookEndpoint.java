/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.polstat.perpustakaan;

import com.polstat.perpustakaan.dto.BookDto;
import com.polstat.perpustakaan.gen.Book;
import com.polstat.perpustakaan.gen.CreateBookRequest;
import com.polstat.perpustakaan.gen.CreateBookResponse;
import com.polstat.perpustakaan.gen.GetBookResponse;
import com.polstat.perpustakaan.gen.SearchBooksRequest;
import com.polstat.perpustakaan.gen.SearchBooksResponse;
import com.polstat.perpustakaan.service.BookService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 *
 * @author IRGI FAHROZI
 */
@Endpoint
public class BookEndpoint {

    private static final String NAMESPACE_URI = "http://www.polstat.com/perpustakaan/gen";

    private BookService bookService;

    @Autowired
    public BookEndpoint(BookService bookService) {
        this.bookService = bookService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBooksRequest")
    @ResponsePayload
    public GetBookResponse getBooks() {
        GetBookResponse response = new GetBookResponse();

        List<BookDto> books = bookService.getBooks();
        for (BookDto book : books) {
            Book buku = new Book();
            buku.setTitle(book.getTitle());
            buku.setAuthor(book.getAuthor());
            buku.setDescription(book.getDescription());

            response.getBook().add(buku);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createBookRequest")
    @ResponsePayload
    public CreateBookResponse createBook(@RequestPayload CreateBookRequest request) {
        BookDto book = BookDto.builder()
                .title(request.getBook().getTitle())
                .author(request.getBook().getAuthor())
                .description(request.getBook().getDescription())
                .build();
        bookService.createBook(book);

        CreateBookResponse response = new CreateBookResponse();
        response.setSuccess(true);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "searchBooksRequest")
    @ResponsePayload
    public SearchBooksResponse searchBooks(@RequestPayload SearchBooksRequest request) {
        String query = request.getQuery();
        List<BookDto> foundBooks = bookService.searchBooksByTitleOrAuthor(query);

        SearchBooksResponse response = new SearchBooksResponse();
        for (BookDto book : foundBooks) {
            com.polstat.perpustakaan.gen.Book buku = new com.polstat.perpustakaan.gen.Book();
            buku.setTitle(book.getTitle());
            buku.setAuthor(book.getAuthor());
            buku.setDescription(book.getDescription());
            response.getBooks().add(buku);
        }

        return response;
    }
}
 

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.polstat.perpustakaan.repository;

import com.polstat.perpustakaan.entity.Book;
import com.polstat.perpustakaan.entity.Member;
import com.polstat.perpustakaan.entity.Pinjam;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author IRGI FAHROZI
 */
@RepositoryRestResource(collectionResourceRel = "pinjam", path = "pinjam")
public interface PinjamRepository extends PagingAndSortingRepository<Pinjam, Long>, CrudRepository<Pinjam, Long> {

    List<Pinjam> findByMember(@Param("member_id") Member member);

    List<Pinjam> findByBuku(@Param("buku_id") Book buku);

    List<Pinjam> findByid(@Param("pinjam_id") Pinjam pinjam);
}

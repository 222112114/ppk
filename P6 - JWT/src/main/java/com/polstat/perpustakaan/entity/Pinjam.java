/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.polstat.perpustakaan.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author IRGI FAHROZI
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "pinjam")
public class Pinjam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_member", referencedColumnName = "id", nullable = false)
    private Member member;
    
    @ManyToOne
    @JoinColumn(name = "id_buku", referencedColumnName = "id", nullable = false)
    private Book buku;
    
    @Temporal(TemporalType.DATE)
    private Date tanggalPinjam;
    
    @Temporal(TemporalType.DATE)
    private Date tanggalKembali;
    
    
    @Column(nullable = false)
    private String statusPinjaman;
    
    @Column(nullable = false)
    private int jumlahHariTelat;
}

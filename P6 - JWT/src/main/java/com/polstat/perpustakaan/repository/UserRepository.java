/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.polstat.perpustakaan.repository;

import com.polstat.perpustakaan.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author IRGI FAHROZI
 */
public interface UserRepository extends JpaRepository<User,Long>{
    public User findByEmail(String Email);
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.calculator.service;

import org.springframework.stereotype.Service;

/**
 *
 * @author IRGI FAHROZI
 */
@Service
public class CalculatorServiceImpl implements CalculatorService{
    
    public double add(double a, double b) {
        return a+b;
    }
    public double subtract(double a, double b) {
        return a-b;
    }
    public double multiply(double a, double b) {
        return a*b;
    }
    public double divide(double a, double b) {
        if (b==0){
            throw new IllegalArgumentException("Cannot divide by zero");
        }
        return a/b;
    }
    
}

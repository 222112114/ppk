/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.calculator.service;

/**
 *
 * @author IRGI FAHROZI
 */
public interface CalculatorService {

    public double add(double a, double b);

    public double subtract(double a, double b);

    public double multiply(double a, double b);

    public double divide(double a, double b);
}

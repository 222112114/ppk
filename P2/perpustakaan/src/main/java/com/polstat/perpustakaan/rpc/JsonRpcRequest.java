/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.polstat.perpustakaan.rpc;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author IRGI FAHROZI
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class JsonRpcRequest {

    private String Jsonrpc;
    private String method;
    private JsonNode params;
    private String id;
}

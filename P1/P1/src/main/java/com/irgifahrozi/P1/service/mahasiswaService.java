/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.irgifahrozi.P1.service;

import com.irgifahrozi.P1.model.mahasiswa;
import java.util.List;

/**
 *
 * @author IRGI FAHROZI
 */
public interface mahasiswaService {
    List<mahasiswa> getAllMahasiswa();
}

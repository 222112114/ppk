/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.irgifahrozi.P1.controller;


import com.irgifahrozi.P1.service.mahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author IRGI FAHROZI
 */
@Controller
public class mahasiswaController {
    @Autowired
    private mahasiswaService mahasiswaService;
    
    //displat list of mahasiswa
    @GetMapping("/")
    public String viewHomePage(Model model){
        model.addAttribute("listMahasiswa",mahasiswaService.getAllMahasiswa());
        return "index";
    }
}

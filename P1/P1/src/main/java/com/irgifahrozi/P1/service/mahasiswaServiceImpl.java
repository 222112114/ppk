/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.irgifahrozi.P1.service;

import com.irgifahrozi.P1.model.mahasiswa;
import com.irgifahrozi.P1.repository.mahasiswaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IRGI FAHROZI
 */
@Service
public class mahasiswaServiceImpl implements mahasiswaService{
    @Autowired
    private mahasiswaRepository mahasiswaRepository;
    
    @Override
    public List<mahasiswa> getAllMahasiswa() {
        return mahasiswaRepository.findAll();
    }
}

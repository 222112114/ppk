/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.irgifahrozi.P1.repository;

import com.irgifahrozi.P1.model.mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author IRGI FAHROZI
 */
@Repository
public interface mahasiswaRepository extends JpaRepository<mahasiswa,String>{
    
}
